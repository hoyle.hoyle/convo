#!/bin/bash

git pull origin 2>&1 | grep "Already up to date."
if [ $? -eq 1 ]
then
	killall server
	cd cmd/server 
	nohup go run . &
else
	echo "no change"
fi
