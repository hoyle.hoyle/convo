package main

import (
	"context"
	"fmt"
	_ "image/png"
	"math/rand"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

const (
	address = "localhost:50051"
	// address     = "178.128.76.179:50051"
	defaultName = "user1234"
	defaultPass = "pass1234"
)

func main() {
	//logger := zerolog.New(os.Stdout).With().Timestamp().Logger()
	logger := log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	// Initialize the app
	clientCtx := &ClientCtx{
		logger,
		NewRoom(),
		"",
		nil,
		ctx,
	}

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	name := fmt.Sprintf("User%d", r.Int()%10000)
	if len(os.Args) > 1 {
		name = os.Args[1]
	}
	logger.Info().Msgf("User: %v", name)

	err := clientCtx.Start(name)
	if err != nil {
		clientCtx.log.Fatal().Msgf("%v", err)
	}
}
