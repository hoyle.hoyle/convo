package main

import (
	"fmt"
	"image"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"runtime"

	"github.com/disintegration/imaging"
	"github.com/golang/freetype/truetype"
	"gocv.io/x/gocv"
	"golang.org/x/image/font"
)

func openbrowser(url string) {
	var err error

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		log.Fatal(err)
	}

}

func loadPicture(path string) (image.Image, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}
	return img, nil
}

func loadTTF(path string, size float64) (font.Face, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	font, err := truetype.Parse(bytes)
	if err != nil {
		return nil, err
	}

	return truetype.NewFace(font, &truetype.Options{
		Size:              size,
		GlyphCacheEntries: 1,
	}), nil
}

func getUserImage() image.Image {
	// parse args
	// deviceID, _ := strconv.Atoi(os.Args[1])
	deviceID := 0
	xmlFile := "face.xml"

	// open webcam
	webcam, err := gocv.VideoCaptureDevice(int(deviceID))
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer webcam.Close()

	// open display window
	// window := gocv.NewWindow("Face Detect")
	// defer window.Close()

	// prepare image matrix
	img := gocv.NewMat()
	defer img.Close()

	// load classifier to recognize faces
	classifier := gocv.NewCascadeClassifier()
	defer classifier.Close()

	if !classifier.Load(xmlFile) {
		fmt.Printf("Error reading cascade file: %v\n", xmlFile)
		return nil
	}

	fmt.Printf("start reading camera device: %v\n", deviceID)
	for {
		if ok := webcam.Read(&img); !ok {
			fmt.Printf("cannot read device %d\n", deviceID)
			return nil
		}
		if img.Empty() {
			continue
		}

		// detect faces
		rects := classifier.DetectMultiScale(img)
		fmt.Printf("found %d faces\n", len(rects))

		// draw a rectangle around each face on the original image,
		// along with text identifying as "Human"
		for _, r := range rects {
			fmt.Println(r)

			r2 := image.Rectangle{r.Min.Add(image.Point{-64, -64}), r.Max.Add(image.Point{64, 64})}

			i, err := img.ToImage()
			if err == nil {
				my_sub_image := i.(interface {
					SubImage(r image.Rectangle) image.Image
				}).SubImage(r2)

				return imaging.Resize(my_sub_image, 64, 64, imaging.Lanczos)
			}

			return nil
		}

		// // show the image in the window, and wait 1 millisecond
		// window.IMShow(img)
		// if window.WaitKey(1) >= 0 {
		// 	break
		// }
	}

	return nil
}
