package main

import (
	"bytes"
	"context"
	"fmt"
	"image"
	"image/jpeg"
	"io"
	"time"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
	"github.com/golang/protobuf/ptypes"
	"github.com/rs/zerolog"
	pb "gitlab.com/hoyle.hoyle/convo/protocol"
	"golang.org/x/image/colornames"
	"golang.org/x/image/font/basicfont"
)

type ClientCtx struct {
	log         zerolog.Logger
	room        Room
	clientUUID  string
	convoClient pb.ConvoClient
	ctx         context.Context
}

func (client *ClientCtx) StartRoomStream(stream pb.Convo_RoomActionClient) {
	for {
		client.log.Info().Msg("Waiting for stream message")
		in, err := stream.Recv()
		if err == io.EOF {
			client.log.Info().Msg("stream closed")
			return
		}
		if err != nil {
			client.log.Error().Msg(err.Error())
			return
		}
		if in.GetJoin() != nil {
			client.log.Info().Msgf("Join: %v", in.GetUuid())

			client.room.CreateUserInRoom(in.GetUuid())

		}
		if in.GetLeave() != nil {
			client.log.Info().Msgf("Leave: %v", in.GetUuid())
			client.room.DeleteUserFromRoom(in.GetUuid())
		}
		if in.GetMove() != nil {
			client.log.Info().Msgf("Move: %v - %f %f",
				in.GetUuid(),
				in.GetMove().GetLocation().GetX(),
				in.GetMove().GetLocation().GetY(),
			)
			client.room.UpdateUserInRoom(
				in.GetUuid(),
				in.GetMove().GetLocation().GetX(),
				in.GetMove().GetLocation().GetY(),
			)
		}
		if in.GetMessage() != nil {
			client.log.Info().Msgf("Message: %v - %v",
				in.GetUuid(),
				in.GetMessage().GetMessage(),
			)
			client.room.UpdateUserText(
				in.GetUuid(),
				in.GetMessage().GetMessage(),
			)
		}
		if in.GetUserData() != nil {
			client.log.Info().Msgf("UserData: %v - %v",
				in.GetUuid(),
				in.GetUserData().GetKey(),
			)
			if in.GetUserData().GetKey() == "image" {
				b := in.GetUserData().GetValue()
				r := bytes.NewReader(b)
				img, _, err := image.Decode(r)
				if err != nil {
					client.log.Error().Msgf("Error decoding image: %v", err)
				} else {
					client.room.UpdateUserImage(
						in.GetUuid(),
						img,
					)
				}
			}
			if in.GetUserData().GetKey() == "name" {
				n := string(in.GetUserData().GetValue())
				client.room.UpdateUserName(
					in.GetUuid(),
					n,
				)
			}
		}
	}
}

func (client *ClientCtx) StartTicks(stream pb.Convo_RoomActionClient) {
	for {
		time.Sleep(time.Second * 10)
		client.log.Info().Msgf("Tick")
		stream.Send(&pb.UserAction{
			Uuid:   client.clientUUID,
			Time:   ptypes.TimestampNow(),
			Action: &pb.UserAction_Tick{},
		})
	}
}

func (client *ClientCtx) Start(name string) error {
	client.log.Info().Msg("convo client - start")
	defer client.log.Info().Msg("convo client - done")

	header, err := client.networkStart(name)
	if err != nil {
		return err
	}

	stream, roomKey, err := client.joinFirstRoom(header)
	if err != nil {
		return err
	}

	go client.StartRoomStream(stream)

	// Join the room
	stream.Send(&pb.UserAction{
		Uuid:   client.clientUUID,
		Time:   ptypes.TimestampNow(),
		Action: &pb.UserAction_Join{Join: &pb.JoinAction{RoomKey: roomKey}},
	})

	// Send your name
	stream.Send(&pb.UserAction{
		Uuid:   client.clientUUID,
		Time:   ptypes.TimestampNow(),
		Action: &pb.UserAction_UserData{UserData: &pb.UserDataAction{Key: "name", Value: []byte(name)}},
	})

	go client.StartTicks(stream)

	fd := NewFaceDetect()

	pixelgl.Run(func() {
		client.log.Info().Msg("Create window")
		cfg := pixelgl.WindowConfig{
			Title:  "Pixel Rocks!",
			Bounds: pixel.R(0, 0, 800, 600),
			VSync:  true,
		}
		win, err := pixelgl.NewWindow(cfg)
		if err != nil {
			panic(err)
		}
		win.SetSmooth(true)

		fd.Start(func(img image.Image) {
			buf := new(bytes.Buffer)
			jpeg.Encode(buf, img, &jpeg.Options{Quality: 65})
			toSend := buf.Bytes()

			// Send your icon
			stream.Send(&pb.UserAction{
				Uuid:   client.clientUUID,
				Time:   ptypes.TimestampNow(),
				Action: &pb.UserAction_UserData{UserData: &pb.UserDataAction{Key: "image", Value: toSend}},
			})
		})

		face, err := loadTTF("/System/Library/Fonts/Supplemental/Courier New.ttf", 11)
		if err != nil {
			client.log.Warn().Msgf("Error loading TTF: %v", err)
			face = basicfont.Face7x13
		}

		basicAtlas := text.NewAtlas(face, text.ASCII)
		client.log.Info().Msgf("Line Height of Atlas: %v", basicAtlas.LineHeight())

		var x, y float64
		var txt string

		for !win.Closed() {
			face := fd.GetLastFace()

			win.Clear(colornames.Skyblue)

			c := win.Bounds().Center()
			c.X += x
			c.Y += y

			if face != nil {
				face.Draw(win, pixel.IM.Moved(c))
			}

			basicTxt := text.New(pixel.V(0, 0), basicAtlas)
			basicTxt.Color = colornames.Black
			txt += win.Typed()
			fmt.Fprintf(basicTxt, "> %s", txt)

			if win.Pressed(pixelgl.KeyEnter) && !win.Repeated(pixelgl.KeyEnter) && txt != "" {
				stream.Send(&pb.UserAction{
					Uuid:   client.clientUUID,
					Time:   ptypes.TimestampNow(),
					Action: &pb.UserAction_Message{Message: &pb.MessageAction{Message: txt}},
				})
				txt = ""
			}

			c.X += 16
			c.Y -= 32
			basicTxt.Draw(win, pixel.IM.Moved(c))

			client.room.IterateUsers(func(user *KnownUser) {
				c := win.Bounds().Center()
				c.X += user.X
				c.Y += user.Y
				if user.image != nil {
					user.image.Draw(win, pixel.IM.Moved(c))
				}

				basicTxt := text.New(pixel.V(0, 0), basicAtlas)
				basicTxt.Color = colornames.Red
				fmt.Fprintf(basicTxt, "%s> %s", user.DisplayName, user.Message)
				c.X += 16
				c.Y -= 32 + 32
				basicTxt.Draw(win, pixel.IM.Moved(c))
			})

			updated := false
			if win.Pressed(pixelgl.KeyLeft) {
				x--
				updated = true
			}
			if win.Pressed(pixelgl.KeyRight) {
				x++
				updated = true
			}
			if win.Pressed(pixelgl.KeyUp) {
				y++
				updated = true
			}
			if win.Pressed(pixelgl.KeyDown) {
				y--
				updated = true
			}
			if win.Pressed(pixelgl.KeyEscape) {
				break
			}
			if updated {
				client.log.Info().Msgf("Move %f %f", x, y)
				stream.Send(&pb.UserAction{
					Uuid: client.clientUUID,
					Time: ptypes.TimestampNow(),
					Action: &pb.UserAction_Move{Move: &pb.MoveAction{
						Location: &pb.Location{
							X: x,
							Y: y,
						},
					}},
				})
			}

			win.Update()
		}
	})

	// Leave the room
	stream.Send(&pb.UserAction{
		Uuid:   client.clientUUID,
		Time:   ptypes.TimestampNow(),
		Action: &pb.UserAction_Leave{Leave: &pb.LeaveAction{}},
	})

	time.Sleep(time.Second * 1)

	stream.CloseSend()

	return nil
}
