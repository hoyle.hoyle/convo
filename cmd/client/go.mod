module gitlab.com:hoyle.hoyle/convo/cmd/client

go 1.15

require (
	github.com/disintegration/imaging v1.6.2
	github.com/faiface/pixel v0.10.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/golang/protobuf v1.4.3
	github.com/rs/zerolog v1.20.0
	gitlab.com/hoyle.hoyle/convo/protocol v0.0.0-20201122040737-334fc2404eeb
	gocv.io/x/gocv v0.25.0
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8
	google.golang.org/grpc v1.33.2
)

replace gitlab.com/hoyle.hoyle/convo/protocol => /Users/jstrohm/code/convo/protocol
