package main

import (
	"fmt"
	"image"
	"math"
	"sync"

	"github.com/disintegration/imaging"
	"github.com/faiface/pixel"
	"gocv.io/x/gocv"
)

type FaceDetect struct {
	RWMutex  sync.RWMutex
	Face     *pixel.Sprite
	onChange func(image.Image)
}

func NewFaceDetect() *FaceDetect {
	return &FaceDetect{
		sync.RWMutex{},
		nil,
		nil,
	}
}

func (f *FaceDetect) Start(onChange func(image.Image)) {
	f.RWMutex.Lock()
	defer f.RWMutex.Unlock()

	f.onChange = onChange

	go f.run()
}

func (f *FaceDetect) Stop() {
	f.RWMutex.Lock()
	defer f.RWMutex.Unlock()
}

func (f *FaceDetect) run() {
	deviceID := 0
	xmlFile := "face.xml"

	// open webcam
	webcam, err := gocv.VideoCaptureDevice(int(deviceID))
	if err != nil {
		fmt.Println(err)
		return
	}
	defer webcam.Close()

	img := gocv.NewMat()
	defer img.Close()

	classifier := gocv.NewCascadeClassifier()
	defer classifier.Close()

	if !classifier.Load(xmlFile) {
		fmt.Printf("Error reading cascade file: %v\n", xmlFile)
		return
	}

	fmt.Printf("start reading camera device: %v\n", deviceID)

	var lastRec image.Rectangle
	var rects []image.Rectangle
	for {
		if ok := webcam.Read(&img); !ok {
			fmt.Printf("cannot read device %d\n", deviceID)
			return
		}
		if img.Empty() {
			continue
		}

		rects = classifier.DetectMultiScaleWithParams(img,
			1.1,
			3,
			0,
			image.Point{128, 128},
			image.Point{img.Size()[0], img.Size()[1]},
		)
		if len(rects) > 0 {
			r := rects[0]

			dx1 := math.Abs(float64(r.Min.X - lastRec.Min.X))
			dy1 := math.Abs(float64(r.Min.Y - lastRec.Min.Y))
			dx2 := math.Abs(float64(r.Max.X - lastRec.Max.X))
			dy2 := math.Abs(float64(r.Max.Y - lastRec.Max.Y))
			if dx1 > 8 || dy1 > 8 || dx2 > 8 || dy2 > 8 {
				lastRec = r
			}
		}

		r := lastRec

		r2 := image.Rectangle{r.Min.Add(image.Point{-64, -64}), r.Max.Add(image.Point{64, 64})}

		i, err := img.ToImage()
		if err == nil {
			my_sub_image := i.(interface {
				SubImage(r image.Rectangle) image.Image
			}).SubImage(r2)

			img := imaging.Resize(my_sub_image, 64, 64, imaging.Gaussian)
			picture := pixel.PictureDataFromImage(img)
			face := pixel.NewSprite(picture, picture.Bounds())

			if f.onChange != nil {
				f.onChange(img)
			}

			f.RWMutex.Lock()
			f.Face = face
			f.RWMutex.Unlock()
		}

	}
}

func (f *FaceDetect) GetLastFace() *pixel.Sprite {
	f.RWMutex.RLock()
	defer f.RWMutex.RUnlock()
	return f.Face
}
