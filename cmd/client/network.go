package main

import (
	"fmt"
	"os"

	pb "gitlab.com/hoyle.hoyle/convo/protocol"
	"google.golang.org/grpc"
)

func createHeader(repHeader *pb.ReplyHeader) *pb.RequestHeader {
	reqHeader := &pb.RequestHeader{
		ClientId: repHeader.GetClientId(),
		Hash:     repHeader.GetHash(),
	}
	return reqHeader
}

func (client *ClientCtx) networkStart(name string) (*pb.ReplyHeader, error) {

	// Connect to the server
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		return nil, fmt.Errorf("did not connect: %v", err)
	}
	client.convoClient = pb.NewConvoClient(conn)

	// Send the client a handshake
	pass := defaultPass
	if len(os.Args) > 2 {
		pass = os.Args[2]
	}

	r, err := client.convoClient.ClientHandshake(client.ctx, &pb.ClientHandshakeRequest{Message: name})
	if err != nil {
		return nil, fmt.Errorf("client handshake failed: %v", name, err)
	}
	client.clientUUID = r.GetHeader().GetClientId().GetUuid()
	client.log.Info().Msgf("Client handshake reply: %s : %s", r.GetMessage(), client.clientUUID)

	// Send username/password
	r2, err := client.convoClient.ClientAuthenticate(client.ctx, &pb.ClientAuthenticateRequest{
		Header: createHeader(r.GetHeader()),
		Key:    name,
		Value:  pass,
	})
	if err != nil {
		return nil, fmt.Errorf("client authenticate failed: %v", name, err)
	}
	client.log.Info().Msg("Client authenticate successful")

	return r2.GetHeader(), nil
}

func (client *ClientCtx) joinFirstRoom(header *pb.ReplyHeader) (pb.Convo_RoomActionClient, string, error) {

	// List the rooms
	r3, err := client.convoClient.ListRooms(client.ctx, &pb.ListRoomsRequest{
		Header: createHeader(header),
	})
	if err != nil {
		return nil, "", fmt.Errorf("list rooms failed: %v", err)
	}
	client.log.Info().Msg("Rooms: ")
	rooms := r3.GetRooms()
	var roomUUID string
	for _, room := range rooms {
		uuid := room.GetUuid()
		name := room.GetName()
		userCount := room.GetUserCount()

		client.log.Info().Msgf("%v : %v Users = %d", name, uuid, userCount)
		roomUUID = uuid
	}

	// Request the key to the room
	keyRequest, err := client.convoClient.RoomKey(client.ctx, &pb.RoomKeyRequest{
		Header:   createHeader(r3.GetHeader()),
		RoomUuid: roomUUID,
	})
	if err != nil {
		return nil, "", fmt.Errorf("join room failed: %v", err)
	}
	roomKey := keyRequest.GetRoomKey()

	stream, err := client.convoClient.RoomAction(client.ctx)
	if err != nil {
		return nil, "", fmt.Errorf("room action failed: %v", err)
	}

	return stream, roomKey, nil
}
