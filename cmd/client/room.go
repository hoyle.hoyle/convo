package main

import (
	"image"
	"sync"

	"github.com/faiface/pixel"
)

type KnownUser struct {
	X           float64
	Y           float64
	UUID        string
	DisplayName string
	Message     string
	image       *pixel.Sprite
}

type Room struct {
	Mutex      sync.Mutex
	KnownUsers map[string]*KnownUser
}

func NewRoom() Room {
	return Room{
		sync.Mutex{},
		make(map[string]*KnownUser, 0),
	}
}

func (r *Room) CreateUserInRoom(uuid string) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	r.KnownUsers[uuid] = &KnownUser{
		X:           0.0,
		Y:           0.0,
		UUID:        uuid,
		DisplayName: "",
	}
}

func (r *Room) DeleteUserFromRoom(uuid string) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	delete(r.KnownUsers, uuid)
}

func (r *Room) getUser(uuid string) *KnownUser {
	var user *KnownUser
	user, ok := r.KnownUsers[uuid]
	if !ok {
		user = &KnownUser{
			X:           0.0,
			Y:           0.0,
			UUID:        uuid,
			DisplayName: "",
			Message:     "",
			image:       nil,
		}
		r.KnownUsers[uuid] = user
	}
	return user
}

func (r *Room) UpdateUserText(uuid string, msg string) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	var user *KnownUser
	user, ok := r.KnownUsers[uuid]
	if !ok {
		user = &KnownUser{
			X:           0.0,
			Y:           0.0,
			UUID:        uuid,
			DisplayName: "",
			Message:     "",
			image:       nil,
		}
		r.KnownUsers[uuid] = user
	}
	user.Message = msg
}

func (r *Room) UpdateUserInRoom(uuid string, x float64, y float64) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	var user *KnownUser
	user, ok := r.KnownUsers[uuid]
	if !ok {
		user = &KnownUser{
			X:           0.0,
			Y:           0.0,
			UUID:        uuid,
			DisplayName: "",
			Message:     "",
			image:       nil,
		}
		r.KnownUsers[uuid] = user
	}
	user.X = x
	user.Y = y
}

func (r *Room) UpdateUserName(uuid string, name string) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	user := r.getUser(uuid)

	user.DisplayName = name

}

func (r *Room) UpdateUserImage(uuid string, img image.Image) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	user := r.getUser(uuid)

	picture := pixel.PictureDataFromImage(img)
	face := pixel.NewSprite(picture, picture.Bounds())

	user.image = face

}

func (r *Room) IterateUsers(f func(*KnownUser)) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	for _, user := range r.KnownUsers {
		f(user)
	}
}
