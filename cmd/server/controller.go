package main

import (
	"context"
	"fmt"
	"io"

	"github.com/google/uuid"

	pb "gitlab.com/hoyle.hoyle/convo/protocol"
)

func (s *ServerCtx) validateHeader(header *pb.RequestHeader) (string, error) {
	clientUUID := header.GetClientId().GetUuid()
	user, ok := s.Users[clientUUID]
	if !ok {
		return "", fmt.Errorf("client does not exist")
	}

	if user.Hash != header.GetHash() {
		return "", fmt.Errorf("hash did not match")
	}

	return clientUUID, nil
}

func (s *ServerCtx) replyHeader(id string) *pb.ReplyHeader {
	hash := uuid.New().String()
	u, ok := s.Users[id]
	if !ok {
		u = UserConnection{Hash: hash, Username: ""}
	}
	u.Hash = hash
	s.Users[id] = u

	return &pb.ReplyHeader{ClientId: &pb.ClientId{Uuid: id}, Hash: hash}
}

func (s *ServerCtx) ClientHandshake(ctx context.Context, in *pb.ClientHandshakeRequest) (*pb.ClientHandshakeReply, error) {
	s.log.Info().Msgf("ClientHandshake: %v", in.GetMessage())

	// Generate new uuid that represents the connection
	id := uuid.New().String()
	s.log.Info().Msgf("new client, assigning id: %v", id)

	return &pb.ClientHandshakeReply{
		Header:  s.replyHeader(id),
		Message: "Hello " + in.GetMessage(),
	}, nil
}

func (s *ServerCtx) ClientAuthenticate(ctx context.Context, in *pb.ClientAuthenticateRequest) (*pb.ClientAuthenticateReply, error) {
	s.log.Info().Msgf("ClientAuthenticate: %v -> %v", in.GetKey(), in.GetValue())
	var clientUUID string
	var err error
	if clientUUID, err = s.validateHeader(in.GetHeader()); err != nil {
		return nil, err
	}

	// if in.GetKey() != "abc1234" && in.GetValue() != "pass1234" {
	// 	delete(s.Users, clientUUID)
	// 	return nil, fmt.Errorf("failed authentication")
	// }

	u := s.Users[clientUUID]
	u.Username = in.GetKey()
	s.Users[clientUUID] = u

	return &pb.ClientAuthenticateReply{
		Header: s.replyHeader(clientUUID),
	}, nil
}

func (s *ServerCtx) ListRooms(ctx context.Context, in *pb.ListRoomsRequest) (*pb.ListRoomsReply, error) {
	s.log.Info().Msgf("ListRooms: ")
	var clientUUID string
	var err error
	if clientUUID, err = s.validateHeader(in.GetHeader()); err != nil {
		return nil, err
	}

	roomInfo := &pb.RoomInfo{Name: "Main Lobby", Uuid: "asdfsdfaf", UserCount: 0}
	return &pb.ListRoomsReply{
		Header: s.replyHeader(clientUUID),
		Rooms:  []*pb.RoomInfo{roomInfo},
	}, nil
}

func (s *ServerCtx) RoomKey(ctx context.Context, in *pb.RoomKeyRequest) (*pb.RoomKeyReply, error) {
	s.log.Info().Msgf("RoomKey: %v", in.GetRoomUuid())
	var clientUUID string
	var err error
	if clientUUID, err = s.validateHeader(in.GetHeader()); err != nil {
		return nil, err
	}

	roomKey := uuid.New().String()

	s.Room.JoinUser(clientUUID, roomKey)

	return &pb.RoomKeyReply{
		Header:  s.replyHeader(clientUUID),
		RoomKey: roomKey,
	}, nil
}

func (s *ServerCtx) RoomAction(stream pb.Convo_RoomActionServer) error {
	// The first message must be a join request for the user so we knw which user the stream is for.
	s.log.Info().Msg("Start Room Action stream")
	var first = true
	for {
		// s.log.Debug().Msg("Waiting for stream message")
		in, err := stream.Recv()
		if err == io.EOF {
			s.log.Info().Msg("stream closed")
			return nil
		}
		if err != nil {
			s.log.Error().Msg(err.Error())
			return err
		}
		// s.log.Info().Msgf("Message received from %v", in.GetUuid())

		if first {
			first = false
			join := in.GetJoin()
			if join == nil {
				s.log.Error().Msgf("The first action to a room must be a join action")
				return fmt.Errorf("The first action to a room must be a join action")
			}
		}

		if err = s.Room.ProcessAction(stream, in); err != nil {
			s.log.Error().Msg(err.Error())
			return err
		}
	}
}
