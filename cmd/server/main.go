package main

import (
	"net"
	"net/http"
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	pb "gitlab.com/hoyle.hoyle/convo/protocol"
	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

type UserConnection struct {
	Hash     string
	Username string
}

type ServerCtx struct {
	pb.UnimplementedConvoServer
	log   zerolog.Logger
	s     *grpc.Server
	Users map[string]UserConnection
	Room  *Room
}

func (service *ServerCtx) Start() {
	service.log.Info().Msg("convo server - start")
	defer service.log.Info().Msg("convo server - done")

	lis, err := net.Listen("tcp", port)
	if err != nil {
		service.log.Fatal().Msgf("failed to listen: %v", err)
	}

	pb.RegisterConvoServer(service.s, service)

	if err := service.s.Serve(lis); err != nil {
		service.log.Fatal().Msgf("failed to serve: %v", err)
	}
}

func web(log zerolog.Logger) {
	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/", fs)

	log.Info().Msg("Listening on :80...")
	err := http.ListenAndServe(":80", nil)
	if err != nil {
		log.Fatal().Msgf("%v", err)
	}
}

func main() {
	//logger := zerolog.New(os.Stdout).With().Timestamp().Logger()
	logger := log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	server := grpc.NewServer()

	// Initialize the app
	serverCtx := &ServerCtx{
		pb.UnimplementedConvoServer{},
		logger,
		server,
		map[string]UserConnection{},
		NewRoom(logger, "Default"),
	}

	go web(logger)

	serverCtx.Start()
}
