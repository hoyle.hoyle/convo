package main

import (
	"sync"

	"github.com/golang/protobuf/ptypes"
	"github.com/rs/zerolog"
	pb "gitlab.com/hoyle.hoyle/convo/protocol"
)

type RoomUser struct {
	userUUID string
	roomKey  string
	Data     map[string][]byte
	stream   pb.Convo_RoomActionServer
}

func NewRoomUser(userUUID string, roomKey string) *RoomUser {
	return &RoomUser{
		userUUID,
		roomKey,
		make(map[string][]byte, 0),
		nil,
	}
}

type Room struct {
	log   zerolog.Logger
	Mutex sync.RWMutex
	Users map[string]*RoomUser
}

func NewRoom(log zerolog.Logger, name string) *Room {
	return &Room{
		log,
		sync.RWMutex{},
		make(map[string]*RoomUser, 0),
	}
}

func (r *Room) JoinUser(userUUID string, roomKey string) error {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	action := &pb.UserAction{
		Time:   ptypes.TimestampNow(),
		Uuid:   userUUID,
		Action: &pb.UserAction_Join{},
	}

	r.echoInRoom(action)
	r.Users[userUUID] = NewRoomUser(userUUID, roomKey)

	return nil
}

func (r *Room) leaveUser(userUUID string) error {
	action := &pb.UserAction{
		Time:   ptypes.TimestampNow(),
		Uuid:   userUUID,
		Action: &pb.UserAction_Leave{},
	}

	r.echoInRoom(action)

	delete(r.Users, userUUID)

	return nil
}

func (r *Room) setUserStream(userUUID string, stream pb.Convo_RoomActionServer) error {
	if user, ok := r.Users[userUUID]; !ok {
		return nil
	} else {
		user.stream = stream
	}

	return nil
}

func (r *Room) echoInRoom(in *pb.UserAction) {
	// Notify everyone in the room about this message
	for id, value := range r.Users {
		if in.GetUuid() != id {
			value.stream.Send(in)
		}
	}
}

func (r *Room) ProcessAction(stream pb.Convo_RoomActionServer, in *pb.UserAction) error {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	if join := in.GetJoin(); join != nil {
		r.log.Info().Msgf("%s: join", in.GetUuid())
		r.setUserStream(in.GetUuid(), stream)

		// Now we need to send info to the newly joined user about everyone
		for id, value := range r.Users {
			for key, value := range value.Data {
				userData := &pb.UserDataAction{Key: key, Value: value}
				action := &pb.UserAction{
					Time:   ptypes.TimestampNow(),
					Uuid:   id,
					Action: &pb.UserAction_UserData{UserData: userData},
				}
				stream.Send(action)
			}
		}

	}
	if leave := in.GetLeave(); leave != nil {
		r.log.Info().Msgf("%s: leave", in.GetUuid())
		r.leaveUser(in.GetUuid())
	}
	if move := in.GetMove(); move != nil {
		r.log.Info().Msgf("%s: move", in.GetUuid())
		r.echoInRoom(in)
	}
	if msg := in.GetMessage(); msg != nil {
		r.log.Info().Msgf("%s: msg: %s", in.GetUuid(), msg.GetMessage())
		r.echoInRoom(in)
	}
	if msg := in.GetUserData(); msg != nil {
		r.log.Info().Msgf("%s: userdata %s: %v", in.GetUuid(), msg.GetKey(), len(msg.GetValue()))
		if u, ok := r.Users[in.GetUuid()]; ok {
			u.Data[msg.GetKey()] = msg.GetValue()
		}
		r.echoInRoom(in)
	}
	return nil
}
