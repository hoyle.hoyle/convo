module gitlab.com:hoyle.hoyle/convo/cmd/server

go 1.15

require (
	github.com/boj/redistore v0.0.0-20180917114910-cd5dcc76aeff // indirect
	github.com/gin-gonic/contrib v0.0.0-20201101042839-6a891bf89f19
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/protobuf v1.4.3
	github.com/google/uuid v1.1.2
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/rs/zerolog v1.20.0
	gitlab.com/hoyle.hoyle/convo/protocol v0.0.0-20201130054939-7ae69a8c2046
	golang.org/x/oauth2 v0.0.0-20180821212333-d2e6202438be
	google.golang.org/genproto v0.0.0-20200806141610-86f49bd18e98 // indirect
	google.golang.org/grpc v1.33.2
)
